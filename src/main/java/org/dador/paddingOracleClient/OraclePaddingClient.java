/*Dhafer BRAHIM // Souhaib ALOUI */
package org.dador.paddingOracleClient;


import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Main Class for Padding OracleClient
 */
public class OraclePaddingClient {
    static final String ENCRYPTED_MESSAGE = "5ca00ff4c878d61e1edbf1700618fb287c21578c0580965dad57f70636ea402fa0017c4acc82717730565174e2e3f713d3921bab07cba15f3197b87976525ce4";
    static final int BLOCK_SIZE = 16;

    /**
     * Fonction takes a number and creates a block of x00 values, padded according to PKCS#7
     * example : n=3 result is 00 00 .. 00 03 03 03
     * @param n : number of bytes of padding
     * @return byte[BLOCK_SIZE] filled with 0 and padding values
     */
    protected byte[] getPaddingArray(int n) {
        byte[] result = new byte[BLOCK_SIZE];

        /**
         * TODO : Your CODE HERE
         */
        return result;
    }

    /**
     * Function that create a modified ciphertext bloc for trying a guess
     * Note that the "ciphertext" correspond to the IV part for the Block Cipher
     * @param ciphertext : original ciphertext bloc
     * @param decoded    : decrypted part of the plain text (for next bloc)
     * @param position   : position of the byte to guess
     * @param guess      : the guess for this query
     * @return a byte array with c0...c(i-1)||ci+i+g||cj+mj+i||...||cn+mn+i
     */
    protected byte[] buildGuessForPosition(byte[] ciphertext, byte[] decoded, int position, byte guess) {
        byte[] result = new byte[BLOCK_SIZE];
      

     for(int i=0; i<BLOCK_SIZE;i++)
     {
    	 
    	 result[i]=ciphertext[i];
    	 
     }
     result[BLOCK_SIZE-1]=(byte)(result[BLOCK_SIZE-1]^(byte)guess);
        return result;
    }

    /**
     * Fonction that splits a message into constituent blocs of BLOCK_SIZE
     *
     * @param message
     * @return an array of blocs
     * @throws IllegalArgumentException
     */
    
    public static String toHexFromByteArray(byte buf[]) {
        StringBuilder strbuf = new StringBuilder(buf.length * 2);
        int i;
        for (i = 0; i < buf.length; i++) {
            if (((int) buf[i] & 0xff) < 0x10) {
                strbuf.append("0");
            }
            strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
        }
        return strbuf.toString();
    }
    
    protected static byte[][] splitMessageIntoBlocks(byte[] message) throws IllegalArgumentException {

       byte[][] blocklist ;

    
       int i,j;
      
       
  
      // message1=toHexFromByteArray(message);
    	if (message.length % BLOCK_SIZE != 0) {
            throw new IllegalArgumentException("Message length is not a multiple of bloc size");
            
        }
    	
    
    int nbBlock = message.length / BLOCK_SIZE ;
    
    blocklist = new byte[nbBlock][BLOCK_SIZE];
    
    for (i=0; i< nbBlock; i++){
    	for (j=0; j< BLOCK_SIZE; j++) {
    		blocklist[i][j] = message[i*BLOCK_SIZE+j] ;
    	}
    }
  

       return blocklist;
    }
    
   
    

    /**
     * Function that takes the 2 last blocks of the message
     * and returns the length of the padding.
     * @param poq : a PaddingOracleQuery object
     * @param previousbloc : next to last block of the ciphertext
     * @param lastbloc : last bloc of the ciphertext
     * @return an integer corresponding to padding length
     * @throws IOException
     * @throws URISyntaxException
     */
    public int getPaddingLengthForLastBlock(PaddingOracleQuery poq, byte[] previousbloc, byte[] lastbloc) throws IOException, URISyntaxException {
       boolean value=false;
       String previous;
       byte[] xor_previousbloc = new byte[previousbloc.length];
       byte[] xor_lastbloc = new byte[previousbloc.length];
       
       for(int i =0 ; i<previousbloc.length;i++){
    	   xor_previousbloc[1]= (byte)(previousbloc[i]^ (byte)01);
    	   xor_lastbloc[1]= (byte)(previousbloc[i]^ lastbloc[i]);
    	   previous = HexConverters.toHexFromByteArray(xor_previousbloc);

    	   
       }
       
    	/**
         * TODO : Your Code HERE
         */
        // should not arrive here !
        return 0;
    }

    /**
     * Main function that takes 2 consecutive blocks of the ciphertext
     * and returns the decryption of the 2nd message block
     *
     * @param poq : a PaddingOracleQuery object to query server
     * @param iv : the "iv" part of the 2 blocks query
     * @param ciphertext : the block that will be decrypted
     * @param padding : set to 0 if not the last block. Set to paddinglength if last block
     * @return a decrypted byte array
     * @throws IOException
     * @throws URISyntaxException
     */
    public byte[] runDecryptionForBlock(PaddingOracleQuery poq, byte[] iv, byte[] ciphertext, int padding) throws IOException, URISyntaxException {
        byte[] decoded = new byte[BLOCK_SIZE];
        if (padding > 0) {
            decoded = getPaddingArray(padding);
        }
        /**
         * TODO : YOUR CODE HERE
         */
        return decoded;
    }

    public static void main(String[] args) {
        OraclePaddingClient opc = new OraclePaddingClient();
        PaddingOracleQuery opq = new PaddingOracleQuery();
       byte [] ENCRYPTED_MESSAGE_BYTE=HexConverters.toByteArrayFromHex(ENCRYPTED_MESSAGE),c;
        byte [][] msg=splitMessageIntoBlocks(ENCRYPTED_MESSAGE_BYTE);
        
        boolean query_result=false;
   /*
        try {
            System.out.println("Server responded : " + opq.query(ENCRYPTED_MESSAGE));
        } catch (Exception e) {
            System.out.print("Exception caught. Server down ?");
            e.printStackTrace();
        }
      */
              try {
        	 for(int g=0;g<256;g++){
        		 
        		 c=opc.buildGuessForPosition(msg[0], null, 0,(byte)g );
        		 String str_c=HexConverters.toHexFromByteArray(c);
        		 
        		 String str_c_msg=HexConverters.toHexFromByteArray(msg[1]);
        		 query_result=opq.query(str_c+str_c_msg);
        				 
        				 if(query_result==true){
        					 System.out.print(str_c);
        					 System.out.print(g);
        				 }
        		 
        	 }} catch (Exception e) {
            System.out.print("Exception caught. Server down ?");
            e.printStackTrace();
        	 }
        	 
        	
        
    }
    }
    


